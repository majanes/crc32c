Based on Stephan Brumme's [Fast CRC32](http://create.stephan-brumme.com/crc32/)
library.

But with modified tables from Intel's
[Slicing-By-8](http://sourceforge.net/projects/slicing-by-8/), to match
SSE4.2's CRC32 instruction, which uses CRC-32C (also known as Castagnoli CRC32).

These files were cloned from Apitrace's thirdparty directory here:
  https://gitlab.freedesktop.org/majanes/apitrace/-/tree/master/thirdparty/crc32c

FrameRetrace Meson build support leveraged this subproject within
Apitrace, but since 0.56, sub-subprojects report an error.
Specifically:

```ERROR: Sandbox violation: Tried to grab file crc32c.c from a different
subproject.```

was generated starting with:

```
  c659be692896f9f36fa46c4955220dc57653f09b
  Author:     Xavier Claessens <xavier.claessens@collabora.com>
  Interpreter: Fix nested subsubproject detection
```

Work around this issue by putting the crc32c lib in it's own git wrap.